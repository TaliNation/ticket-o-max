using NUnit.Framework;
using AlfaBureau.TicketOMax.Server.RequestObjects;

namespace AlfaBureau.TicketOMax.Server.Tests.TestCases.RequestObjects
{
    [TestFixture]
    public class PrivilegeChangeRequestTest
    {
        [TestCase(1, true, true)]
        [TestCase(2, false, true)]
        public void IsValid(int userId, bool isPromotion, bool expected)
        {
            var value = new PrivilegeChangeRequest { UserId = userId, IsPromotion = isPromotion };

            Assert.AreEqual(expected, value.IsValid);
        }
    }
}