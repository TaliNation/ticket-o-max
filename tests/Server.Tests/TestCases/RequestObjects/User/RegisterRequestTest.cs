using NUnit.Framework;
using AlfaBureau.TicketOMax.Server.RequestObjects;

namespace AlfaBureau.TicketOMax.Server.Tests.TestCases.RequestObjects
{
    [TestFixture]
    public class RegisterRequestTest
    {
        [TestCase("jean.luc@gmail.com", "password", "Boudin", "Jean-Luc", true)]
        [TestCase("thomas.goum@mail.com", "Password589*", "Goum", "Thomas", true)]

        [TestCase("", "password", "Boudin", "Jean-Luc", false)]
        [TestCase(null, "password", "Boudin", "Jean-Luc", false)]
        [TestCase("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "password", "Boudin", "Jean-Luc", false)]
        [TestCase("jeanluc@gmail.com", "password", "Boudin", "Jean-Luc", true)]
        [TestCase("jean.luc@gmail", "password", "Boudin", "Jean-Luc", true)]
        [TestCase("jean.luc", "password", "Boudin", "Jean-Luc", false)]

        [TestCase("jean.luc@gmail.com", "", "Boudin", "Jean-Luc", false)]
        [TestCase("jean.luc@gmail.com", null, "Boudin", "Jean-Luc", false)]

        [TestCase("jean.luc@gmail.com", "password", "", "Jean-Luc", false)]
        [TestCase("jean.luc@gmail.com", "password", null, "Jean-Luc", false)]
        [TestCase("jean.luc@gmail.com", "password", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "Jean-Luc", false)]

        [TestCase("jean.luc@gmail.com", "password", "Boudin", "", false)]
        [TestCase("jean.luc@gmail.com", "password", "Boudin", null, false)]
        [TestCase("jean.luc@gmail.com", "password", "Boudin", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", false)]
        public void IsValid(string email, string password, string lastname, string firstname, bool expected)
        {
            var value = new RegisterRequest
            {
                Email = email,
                Password = password,
                Lastname = lastname,
                Firstname = firstname
            };

            Assert.AreEqual(expected, value.IsValid);
        }
    }
}