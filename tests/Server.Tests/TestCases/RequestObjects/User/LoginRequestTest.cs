using NUnit.Framework;
using AlfaBureau.TicketOMax.Server.RequestObjects;

namespace AlfaBureau.TicketOMax.Server.Tests.TestCases.RequestObjects
{
    [TestFixture]
    public class LoginRequestTest
    {
        [TestCase("jean.luc@gmail.com", "password", true)]
        [TestCase("thomas.goum@mail.com", "Password589*", true)]
        [TestCase("", "password", false)]
        [TestCase(null, "password", false)]
        [TestCase("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "password", false)]
        [TestCase("jean.luc@gmail.com", "", false)]
        [TestCase("jean.luc@gmail.com", null, false)]
        [TestCase("jean.luc@gmail.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", false)]
        public void IsValid(string email, string password, bool expected)
        {
            var value = new LoginRequest { Email = email, Password = password };

            Assert.AreEqual(expected, value.IsValid);
        }
    }
}