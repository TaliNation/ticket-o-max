using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.Helpers.Configurations;
using AlfaBureau.TicketOMax.Server.Services;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using FluentAssertions;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using System;

namespace AlfaBureau.TicketOMax.Server.Tests.TestCases.Services
{
    [TestFixture]
    public class UserServiceTest
    {
        private IDbContext DbContext;
        private JwtSettings JwtSettings;
        private UserService TestSubject;

        [SetUp]
        public void SetUp()
        {
            DbContext = new MockDbContext();
            JwtSettings = new JwtSettings() { Secret = "8bPYth3H8g7e3PyEoHhcLYY6D5KfrkYBDsgbbyRa" };

            IOptions<JwtSettings> options = Options.Create(JwtSettings);

            TestSubject = new UserService(options, DbContext);
        }

        [Test]
        public void Authenticate()
        {
            var model = new LoginRequest
            {
                Email = "jean.luc@gmail.com",
                Password = "password"
            };

            string actual = TestSubject.Authenticate(model);

            Assert.NotNull(actual);
        }

        [Test]
        public void Authenticate_WrongEmail()
        {
            var model = new LoginRequest
            {
                Email = "jean.luc@gmail.fr",
                Password = "password"
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.Authenticate(model));
            Assert.AreEqual(exception.Message, "Wrong email or password");
        }

        [Test]
        public void Authenticate_WrongPassword()
        {
            var model = new LoginRequest
            {
                Email = "jean.luc@gmail.com",
                Password = "passwourd"
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.Authenticate(model));
            Assert.AreEqual(exception.Message, "Wrong email or password");
        }

        [Test]
        public void Authenticate_NullModel()
        {
            LoginRequest model = null;
            var exception = Assert.Throws<ArgumentException>(() => TestSubject.Authenticate(model));
            Assert.AreEqual(exception.Message, "Invalid data");
        }

        [Test]
        public void Authenticate_InvalidModel()
        {
            var model = new LoginRequest
            {
                Email = "",
                Password = ""
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.Authenticate(model));
            Assert.AreEqual(exception.Message, "Invalid data");
        }

        [Test]
        public void Register()
        {
            var model = new RegisterRequest
            {
                Email = "papa.daisuki@gmail.com",
                Firstname = "John",
                Lastname = "Teds",
                IsModerator = false,
                Password = "foil123"
            };

            TestSubject.CreateUser(model, 1);
            Assert.True(true);
        }

        [Test]
        public void CreateUser()
        {
            var model = new RegisterRequest
            {
                Email = "papa.daisuki@gmail.com",
                Firstname = "John",
                Lastname = "Teds",
                IsModerator = false,
                Password = "foil123"
            };

            TestSubject.CreateUser(model, 1);
            Assert.True(true);
        }

        [Test]
        public void CreateUser_NullModel()
        {
            RegisterRequest model = null;

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.CreateUser(model, 1));
            Assert.AreEqual(exception.Message, "Invalid data");
        }

        [Test]
        public void CreateUser_InvalidModel()
        {
            var model = new RegisterRequest
            {
                Email = ""
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.CreateUser(model, 1));
            Assert.AreEqual(exception.Message, "Invalid data");
        }

        [Test]
        public void CreateUser_AlreadyExists()
        {
            var model = new RegisterRequest
            {
                Email = "jean.luc@gmail.com",
                Password = "password",
                Firstname = "Jean-Luc",
                Lastname = "Sale",
                IsModerator = false
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.CreateUser(model, 1));
            Assert.AreEqual(exception.Message, $"User with email {model.Email} already exists");
        }

        [Test]
        public void GetAllUsers()
        {
            var expected = new List<int>() { 1, 2, 3 };

            var actual = TestSubject.GetAllUsers().Select(x => x.Id_User);

            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public void ChangeUserPrivilege()
        {
            var model = new PrivilegeChangeRequest
            {
                IsPromotion = true,
                UserId = 1
            };

            TestSubject.ChangeUserPrivilege(model, 1);
            Assert.True(true);
        }

        [Test]
        public void ChangeUserPrivilege_NullModel()
        {
            PrivilegeChangeRequest model = null;

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.ChangeUserPrivilege(model, 1));
            Assert.AreEqual(exception.Message, "Invalid data");
        }

        [Test]
        public void ChangeUserPrivilege_NotFound()
        {
            var model = new PrivilegeChangeRequest
            {
                IsPromotion = false,
                UserId = 999
            };

            var exception = Assert.Throws<ArgumentException>(() => TestSubject.ChangeUserPrivilege(model, 1));
            Assert.AreEqual(exception.Message, "User to change not found");
        }

        [Test]
        public void GetAllPrivileges()
        {
            var expected = new List<int>() { 1, 2, 3 };

            var actual = TestSubject.GetAllPrivileges().Select(x => x.Id_Privilege);

            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public void GetUserById()
        {
            const string expected = "jean.luc@gmail.com";

            var actual = TestSubject.GetUserById(1).Email;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetUserById_NotFound()
        {
            var actual = TestSubject.GetUserById(999);

            Assert.IsNull(actual);
        }

        [Test]
        public void GetUserWithPrivilegeById()
        {
            const string expected = "jean.luc@gmail.com";

            var actual = TestSubject.GetUserWithPrivilegeById(1).Email;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetUserWithPrivilegeById_NotFound()
        {
            var actual = TestSubject.GetUserById(999);

            Assert.IsNull(actual);
        }
    }
}