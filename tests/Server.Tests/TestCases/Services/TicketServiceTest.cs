using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.Server.Services;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess;
using FluentAssertions;
using NUnit.Framework;

namespace AlfaBureau.TicketOMax.Server.Tests.TestCases.Services
{
    [TestFixture]
    public class TicketServiceTest
    {
        private IDbContext DbContext;
        private TicketService TestSubject;

        [SetUp]
        public void SetUp()
        {
            DbContext = new MockDbContext();
            TestSubject = new TicketService(DbContext);
        }

        [Test]
        public void GetAllTickets()
        {
            var expected = new List<int>() { 1, 2, 3 };

            var actual = TestSubject.GetAllTickets().Select(x => x.Id_Ticket);

            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public void GetAllSupports()
        {
            var expected = new List<int>() { 1, 2, 3 };

            var actual = TestSubject.GetAllSupports().Select(x => x.Id_Support);

            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public void GetAllCategories()
        {
            var expected = new List<int>() { 1, 2, 3 };

            var actual = TestSubject.GetAllCategories().Select(x => x.Id_Category);

            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public void GetTicket()
        {
            const string expected = "SDHTAZP57AJM";

            var actual = TestSubject.GetTicket(1).Code;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetTicket_NotFound()
        {
            var actual = TestSubject.GetTicket(999);

            Assert.IsNull(actual);
        }

        [Test]
        public void DesactivateTicket()
        {
            TestSubject.DesactivateTicket(1, 1);

            Assert.IsTrue(true);
        }
    }
}