using System;
using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.Shared.Constants;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories;

#pragma warning disable RCS1079

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess
{
    public sealed class MockDbContext : IDbContext
    {
        public ICategoryRepository Category { get; }
        public IEventTypeRepository EventType => throw new NotImplementedException();
        public IInterventionRepository Intervention => throw new NotImplementedException();
        public IPrivilegeRepository Privilege { get; }
        public IProgressHistoryRepository ProgressHistory => throw new NotImplementedException();
        public IStatusRepository Status => throw new NotImplementedException();
        public ISupportRepository Support { get; }
        public ITicketRepository Ticket { get; }
        public IUserRepository User { get; }
        public IUserViewRepository UserView { get; }
        public ITicketViewRepository TicketView { get; }

        public List<PrivilegeEntity> PrivilegeEntities { get; set; } = new()
        {
            new PrivilegeEntity { Id_Privilege = 1, Label = PrivilegeConstants.Administrator },
            new PrivilegeEntity { Id_Privilege = 2, Label = PrivilegeConstants.Moderator },
            new PrivilegeEntity { Id_Privilege = 3, Label = PrivilegeConstants.User }
        };

        public List<UserEntity> UserEntities { get; set; } = new()
        {
            new UserEntity { Id_User = 1, Email = "jean.luc@gmail.com", Firstname = "Jean-Luc", Lastname = "Sale", Password = "$2y$10$eaQ7.PO0m4VuXJuOzsq6BOYgjlJYabdNGPPZOg/1p2U6tQVrWx8xS", Id_Privilege = 3 }, // password
            new UserEntity { Id_User = 2, Email = "thomas.goum@mail.com", Firstname = "Thomas", Lastname = "Goum", Password = "$2y$10$01uzhvsHaxdT5TszHWuJx.3qp9rlRYQY...rNlq.71SjVvUueJXt6", Id_Privilege = 1 }, // admin
            new UserEntity { Id_User = 3, Email = "merlin.lenchanteur@orange.fr", Firstname = "Bruno", Lastname = "Loiseau", Password = "$2y$10$PkCzucWJ0qUYRVvGgh64ye2BxGms995dTalEPWHbtl.CMrD4ACCT2", Id_Privilege = 2 }, // modpls454
        };

        public List<TicketEntity> TicketEntities { get; set; } = new()
        {
            new TicketEntity { Id_Ticket = 1, Code = "SDHTAZP57AJM", Date_Issue = DateTime.Parse("2021-04-27 08:30"), Description = "Lorem ipsum dolor sit amet", Title = "Fumée noire qui sort de mon PC" }
        };

        public List<UserViewEntity> UserViewEntities { get; set; } = new()
        {
            new UserViewEntity { Id_User = 1, Email = "jean.luc@gmail.com", Firstname = "Jean-Luc", Lastname = "Sale", Password = "$2y$10$eaQ7.PO0m4VuXJuOzsq6BOYgjlJYabdNGPPZOg/1p2U6tQVrWx8xS", Id_Privilege = 3, Label_Privilege = "User" }, // password
            new UserViewEntity { Id_User = 2, Email = "thomas.goum@mail.com", Firstname = "Thomas", Lastname = "Goum", Password = "$2y$10$01uzhvsHaxdT5TszHWuJx.3qp9rlRYQY...rNlq.71SjVvUueJXt6", Id_Privilege = 1, Label_Privilege = "Administrator" }, // admin
            new UserViewEntity { Id_User = 3, Email = "merlin.lenchanteur@orange.fr", Firstname = "Bruno", Lastname = "Loiseau", Password = "$2y$10$PkCzucWJ0qUYRVvGgh64ye2BxGms995dTalEPWHbtl.CMrD4ACCT2", Id_Privilege = 2, Label_Privilege = "Moderator" }, // modpls454
        };

        public List<CategoryEntity> CategoryEntities { get; set; } = new()
        {
            new CategoryEntity { Id_Category = 1, Label = "PC" },
            new CategoryEntity { Id_Category = 2, Label = "Windows" },
            new CategoryEntity { Id_Category = 3, Label = "Imprimante" }
        };

        public List<SupportEntity> SupportEntities { get; set; } = new()
        {
            new SupportEntity { Id_Support = 1, Label = "Client" },
            new SupportEntity { Id_Support = 2, Label = "Serveur" },
            new SupportEntity { Id_Support = 3, Label = "Imprimante" },
        };

        public List<TicketViewEntity> TicketViewEntities { get; set; } = new()
        {
            new TicketViewEntity { Id_Ticket = 1, Code = "SDHTAZP57AJM" },
            new TicketViewEntity { Id_Ticket = 2, Code = "AD5F6Q3FG6EZ" },
            new TicketViewEntity { Id_Ticket = 3, Code = "EF5478AS5D2W" },
        };

        public MockDbContext()
        {
            Privilege = new MockPrivilegeRepository(PrivilegeEntities);
            User = new MockUserRepository(UserEntities);
            Ticket = new MockTicketRepository(TicketEntities);
            UserView = new MockUserViewRepository(UserViewEntities);
            Category = new MockCategoryRepository(CategoryEntities);
            Support = new MockSupportRepository(SupportEntities);
            TicketView = new MockTicketViewRepository(TicketViewEntities);
        }
    }
}