using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockUserViewRepository : IUserViewRepository
    {
        public IRepository<UserViewEntity> General { get; }
        public ICrudRepository<UserViewEntity, int> Crud { get; }

        public MockUserViewRepository(List<UserViewEntity> entities)
        {
            General = new MockRepository<UserViewEntity>(entities);
            Crud = new MockSingleIdCrudRepository<UserViewEntity, int>(entities, "Id_Ticket");
        }
    }
}