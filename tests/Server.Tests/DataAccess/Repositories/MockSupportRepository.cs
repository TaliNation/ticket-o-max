using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockSupportRepository : ISupportRepository
    {
        public IRepository<SupportEntity> General { get; }
        public ICrudRepository<SupportEntity, int> Crud { get; }

        public MockSupportRepository(List<SupportEntity> entities)
        {
            General = new MockRepository<SupportEntity>(entities);
            Crud = new MockSingleIdCrudRepository<SupportEntity, int>(entities, "Id_Ticket");
        }
    }
}