using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockTicketRepository : ITicketRepository
    {
        public IRepository<TicketEntity> General { get; }
        public ICrudRepository<TicketEntity, int> Crud { get; }

        public MockTicketRepository(List<TicketEntity> entities)
        {
            General = new MockRepository<TicketEntity>(entities);
            Crud = new MockSingleIdCrudRepository<TicketEntity, int>(entities, "Id_Ticket");
        }
    }
}