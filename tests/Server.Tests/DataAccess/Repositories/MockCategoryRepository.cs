using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockCategoryRepository : ICategoryRepository
    {
        public IRepository<CategoryEntity> General { get; }
        public ICrudRepository<CategoryEntity, int> Crud { get; }

        public MockCategoryRepository(List<CategoryEntity> entities)
        {
            General = new MockRepository<CategoryEntity>(entities);
            Crud = new MockSingleIdCrudRepository<CategoryEntity, int>(entities, "Id_Ticket");
        }
    }
}