using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

#pragma warning disable RCS1079

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockTicketViewRepository : ITicketViewRepository
    {
		private readonly List<TicketViewEntity> Entities;

        public IRepository<TicketViewEntity> General { get; }
        public ICrudRepository<TicketViewEntity, int> Crud { get; }

        public MockTicketViewRepository(List<TicketViewEntity> entities)
        {
			Entities = entities;
            General = new MockRepository<TicketViewEntity>(entities);
            Crud = new MockSingleIdCrudRepository<TicketViewEntity, int>(entities, "Id_Ticket");
        }
		public IEnumerable<TicketViewEntity> GetAllActivated()
		{
			return Entities.Where(x => x.Date_Delete == null).ToList();
		}
	}
}