using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

#pragma warning disable RCS1079

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core
{
    public class MockRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        private readonly List<TEntity> Entities;

        public MockRepository(List<TEntity> entities)
        {
            Entities = entities;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Entities;
        }

        public IEnumerable<TEntity> GetByColumn(string columnName, IComparable value)
        {
            var entityType = typeof(TEntity);

            var entityProperty = entityType.GetProperty(columnName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            var propertyType = entityProperty.PropertyType;

            var parameter = Expression.Parameter(entityType);

            if(!(propertyType.IsValueType || propertyType.Equals(typeof(string))))
            {
                throw new Exception("Value is not comparable");
            }

            var condition = Expression.Lambda<Func<TEntity, bool>>
            (
                Expression.Equal
                (
                    Expression.Property(parameter, entityProperty),
                    Expression.Constant(value)
                ),
                parameter
            ).Compile();

            return Entities.Where(condition);
        }

        public IEnumerable<TEntity> GetByColumn(Dictionary<string, IComparable> columnValuePairs)
        {
            throw new System.NotImplementedException();
        }
    }
}