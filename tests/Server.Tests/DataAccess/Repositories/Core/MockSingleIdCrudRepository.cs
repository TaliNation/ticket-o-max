using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core
{
    public class MockSingleIdCrudRepository<TEntity, TPrimaryKey> : ICrudRepository<TEntity, TPrimaryKey>
        where TEntity : IEntity
        where TPrimaryKey : IComparable
    {
        private readonly string PrimaryKeyName;

        private readonly List<TEntity> Entities;

        public MockSingleIdCrudRepository(List<TEntity> entities, string primaryKeyName)
        {
            Entities = entities;
            PrimaryKeyName = primaryKeyName;
        }

        public void Add(object entity)
        {
        }

        public TPrimaryKey AddGetId(object entity)
        {
            return default;
        }

        public TEntity GetById(TPrimaryKey id)
        {
            var entityType = typeof(TEntity);

            var entityProperty = entityType.GetProperty(PrimaryKeyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            var propertyType = entityProperty.PropertyType;

            var parameter = Expression.Parameter(entityType);

            if(!(propertyType.IsValueType || propertyType.Equals(typeof(string))))
            {
                throw new Exception("Value is not comparable");
            }

            var condition = Expression.Lambda<Func<TEntity, bool>>
            (
                Expression.Equal
                (
                    Expression.Property(parameter, entityProperty),
                    Expression.Constant(id)
                ),
                parameter
            ).Compile();

            return Entities.SingleOrDefault(condition);
        }

        public void Remove(TPrimaryKey id)
        {
            var entityToRemove = GetById(id);
            Entities.Remove(entityToRemove);
        }

        public void Update(TPrimaryKey id, object entity)
        {
        }
    }
}