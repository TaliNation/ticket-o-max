using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockUserRepository : IUserRepository
    {
        public IRepository<UserEntity> General { get; }
        public ICrudRepository<UserEntity, int> Crud { get; }

        public MockUserRepository(List<UserEntity> entities)
        {
            General = new MockRepository<UserEntity>(entities);
            Crud = new MockSingleIdCrudRepository<UserEntity, int>(entities, "Id_User");
        }
    }
}