using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.Server.Tests.DataAccess.Repositories
{
    public class MockPrivilegeRepository : IPrivilegeRepository
    {
        public IRepository<PrivilegeEntity> General { get; }

        public ICrudRepository<PrivilegeEntity, int> Crud { get; }

        public MockPrivilegeRepository(List<PrivilegeEntity> entities)
        {
            General = new MockRepository<PrivilegeEntity>(entities);
            Crud = new MockSingleIdCrudRepository<PrivilegeEntity, int>(entities, "Id_Privilege");
        }
    }
}