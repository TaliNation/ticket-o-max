# Services
Les services sont les classes dans lesquelles sont toutes les règles métiers de l'application, à savoir :
- La validation des données.
- La récupération de données de la base.
- Les traitements.
- La modification de données dans la base.

Ils terminent tous par le suffixe Service, héritent de <xref href="AlfaBureau.TicketOMax.Server.Services.Core.IService?alt=IService"/>, et sont contenues dans l'espace de nom <xref href="AlfaBureau.TicketOMax.Server.Services?alt=IService"/>.

Comme ils contiennent la logique métier, il est impératif qu'ils soient testés unitairement.

Tous les services sont doublés d'une interface afin de faciliter les tests unitaires des contrôleurs.

<img src="../images/Service.png" />
