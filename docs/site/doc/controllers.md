# Contrôleurs
Les contrôleurs sont les classes contenant les API REST qui sont pointes d'entrée du serveur depuis l'extérieur. Leurs noms se terminent toujours par *Controller*, ils héritent tous de la classe d'ASP.NET *BaseController* et se trouvent dans l'espace de nom <xref href="AlfaBureau.TicketOMax.Server.Controllers?alt=AlfaBureau.TicketOMax.Server.Controllers"/>

Les contrôleurs se chargent uniquement de la réception des données du client et de sa réponse, toutes les règles métiers sont déléguées aux services. Ainsi, quasiment tous les contrôleurs utilisent au moins un service.

Il faut considérer l'utilisation des attributs suivants sur toutes les méthodes des contrôleurs :
- Méthode HTTP : [HttpGet], [HttpPost], [HttpPut] et [HttpDelete]
- Format de réponses : toujours [Produces("application/json")]
- Route : l'adresse à laquelle la méthode va se lancer. Elle se couple avec l'attribut du contrôleur, ainsi, un contrôleur avec [Route("Ticket")] et une méthode avec [Route("AllTickets")] donnera l'URI Ticket/AllTickets
- Authorization : toutes les API nécessitant que l'utilisateur soit authentifié doivent avoir l'attribut [Authorize]. Les paramètres sont réservés pour les API nécessitant des droits plus élevés.

<img src="../images/Controller.png" />
