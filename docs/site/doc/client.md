# Client
Le projet Client contient le code qui s'exécute sur le navigateur du client. Il ne contient pas de règles métiers et de contente de manipuler l'interface graphique, et de faire des appels au serveur.

Blazor WASM utilise le principe de DOM virtuel, une "fausse" page HTML qui compile en réelle page HTML. L'intéret est qu'il propose beaucoup plus de fonctionnalités que le DOM classique, telle que la notion de composants (portions de DOM réutilisables), de layout (parties de HTML en englobant d'autres), ou encore l'utilisation de code en C#.

Ce projet repose sur l'utilisation de plusieurs concepts expliqués sur les liens suivants :
- [Hiérarchie des composants](components.md)
- [Organisation du code](razor.md)