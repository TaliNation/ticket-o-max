# Introduction
Ticket O'Max est une application web divisée en plusieurs parties, suivant le principe d'architecture 3 tiers avec :
- Le client s'exécutant sur le navigateur web de l'utilisateur, qui s'occupe de l'interface graphique et la présentation des données.
- Le serveur, qui s'occupe de la récupération et du traitement des données envoyées par l'utilisateur et récupérées de la base de données.
- La base de données hébergée sur nos serveurs.

## Technologies utilisées
### Base de données
**Microsoft SQL Server**

### Serveur
**ASP.NET 5** : un framework de développement web backend basé sur l'environnement .NET, permettant d'héberger des API REST. 

Ces API sont capables de lire des paramètres de lire les données provenant de requêtes et de retourner des réponses dans des formats standardisés, tels que JSON, ce qui les rend exploitables depuis n'importe quel type de client (navigateur, application mobile...).

### Client
**Blazor WASM** : un framework de développement web côté client basé sur l'environnement .NET. 

Il permet de générer du code Web Assembly, un langage de très bas niveau pouvant être lu par tous les navigateurs modernes et réputé pour ses performances, et sa facilité à être généré à partir d'un autre langage (C#, Rust, Python...), ce qui en fait un concurrent direct de JavaScript. 

Ainsi, dans sa forme déployée, une application Blazor ne contient plus aucune trace de .NET et ne nécessite pas d'installer un runtime pour être exécuté.

## Architecture
La solution Ticket O'Max est divisée en 4 projets :
- **Client** : le projet Blazor WASM, contenant les pages de l'application.
- **Server** : le projet ASP.NET, contenant les API.
- **DataAccess** : le projet prenant en charge toutes les interactions avec la base de données.
- **Shared** : un projet contenant toutes les ressources partagées (constantes, fonctions...) entre les différents projets.

Le graphique de dépendance des différents projets est le suivant :

<img src="../images/Dependencies.png">

Explications : 
- Server ne fait aucune requête à la base de données et se contente d'appeler DataAccess (via [DbContext](dbcontext.md))
- Client ne dépends pas de Server, mais consomme ses API via HTTP.
- Shared ne contient que des ressources de bas niveau et ne dépend d'aucun projet.
