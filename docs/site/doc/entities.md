# Entitées
Les entités sont des classes POCO (sans comportement) ayant pour but de mapper les tables de la base de données. Leur noms se terminent toujours par le suffixe Entity, elles héritent de <xref href="AlfaBureau.TicketOMax.DataAccess.Entities.IEntity?alt=IEntity"/> et sont contenues dans le namespace <xref href="AlfaBureau.TicketOMax.DataAccess.Entities?alt=AlfaBureau.TicketOMax.DataAccess.Entities"/>

Ce sont ces classes qui sont retournées par les [dépots](repositories.md), ce qui permet de les utiliser facilement dans le programme.

Le nom des propriétés ne respecte pas forcement la charte de développement car ils doivent correspondre aux nom des colonnes dans la base de données.

<img src="../images/Entity.png" />