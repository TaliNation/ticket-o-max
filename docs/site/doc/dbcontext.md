# Contexte de base de données
Le contexte (Database Context) est une classe contenant la liste de tous les dépôts, permettant ainsi d'accéder à toutes les tables de la base de données à partir d'un seul objet. 

C'est le point d'entrée du projet DataAccess, car c'est à travers de lui que transiteront toutes les requêtes du serveur vers la base de données.

Il est doublé d'une interface pour faciliter les tests unitaires.

<img src="../images/DbContext.png" />
