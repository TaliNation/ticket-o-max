# Server
Le projet Server est l'interface permettant d'exposer les données de la base et traiter les requêtes des clients.

Il prend la forme d'un projet d'API REST, et est accessible depuis n'importe quel client via HTTP, ce qui permet d'utiliser même en dehors du contexte de Ticket O'Max.

Ce projet repose sur l'utilisation de plusieurs [patrons de conception](https://fr.wikipedia.org/wiki/Patron_de_conception) expliqués sur les liens suivants :
- [Services](services.md)
- [Controllers](controllers.md)
