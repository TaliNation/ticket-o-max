# Hiérarchie des composants
Lors de l'exécution du client Blazor, le HTML généré est la combinaison de plusieurs composants suivant une hiérarchie stricte, tel qu'expliqué sur le schéma suivant :

<img src="../images/ComponentHierarchy.png" />
