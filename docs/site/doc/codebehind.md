# Organisation du code
Le code de chaque composant Blazor est divisé en 3 parties :
- La structure, contenue dans les fichiers .razor.
- Le style, contenu dans les fichiers .razor.css
- Le comportement, contenu dans les fichiers .razor.cs

Concernant le style, Blazor se charge automatiquement d'associer un fichier de style .razor.css à son composant .razor en se basant sur le nom. Le style est local au composant et accessible nulle part ailleurs.

Concernant le comportement, il s'agit d'une classe C# (UserListBase) faisant l'interface entre le composant (UserList.razor) et la classe de base de Blazor permettant de définir un composant (ComponentBase). UserList hérite de UserListBase qui hérite de ComponentBase.

<img src="../images/Page.png" />

Pour les composants les plus simple (sans structure ou sans comportement), il est autorisé de mettre tout le code dans le fichier .razor.
