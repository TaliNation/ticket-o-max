# DataAccess
Il s'agit du projet qui va faire office d'interface entre la base de données et le serveur afin de :
- Masquer la complexité (requêtes SQL, mappage des données sur les objets...).
- Savoir où intervenir en cas d'erreur concernant l'accès aux données.
- Ne jamais avoir à modifier le code de Server si jamais la structure de la base de données venait à changer.

Ce projet repose sur l'utilisation de plusieurs [patrons de conception](https://fr.wikipedia.org/wiki/Patron_de_conception) afin de rendre l'API compréhensible, mais surtout facilement exploitable depuis Server.

Les différents patrons sont expliqués sur les liens suivants :
- [Entities](entities.md)
- [Repositories](repositories.md)
- [Database Context](dbcontext.md)
