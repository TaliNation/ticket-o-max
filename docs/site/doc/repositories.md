# Dépôts
Les [dépôts (Repositories)](https://martinfowler.com/eaaCatalog/repository.html) sont les classes contenant toute la logique d'accès à la base de données. Leurs noms se terminent toujours par le suffixe Repository le namespace <xref href="AlfaBureau.TicketOMax.DataAccess.Repositories?alt=AlfaBureau.TicketOMax.DataAccess.Repositories"/>.

Il y a deux types de dépôt :
- Les dépôts génériques (<xref href="AlfaBureau.TicketOMax.DataAccess.Repositories.Core?alt=Repositories.Core"/>) qui contiennent une logique d'accès aux données non spécifiques à une table en particulier.
- Les dépôts spécifiques (<xref href="AlfaBureau.TicketOMax.DataAccess.Repositories?alt=Repositories"/>), un par table, pour les accès plus spécifiques. Ils utilisent les dépôts génériques afin d'éviter la répétition de code inutile.

Les dépôts sont toujours doublés d'une interface pour faciliter les tests unitaires.

<img src="../images/Repository.png" />