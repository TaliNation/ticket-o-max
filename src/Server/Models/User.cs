using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.Helpers.Configurations;
using Microsoft.IdentityModel.Tokens;

namespace AlfaBureau.TicketOMax.Server.Models
{
    /// <summary>Données et règles métier concernant les utilisateurs</summary>
    public class User
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }

        public User(UserEntity entity)
        {
            Id = entity.Id_User;
            Lastname = entity.Lastname;
            Firstname = entity.Firstname;
            Email = entity.Email;
        }

        /// <summary>Génération d'un JSON Web Token</summary>
        /// <param name="settings">Configuration contenant la clef de génération</param>
        public string GerenateJwtToken(JwtSettings settings)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(settings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new [] { new Claim("id", Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}