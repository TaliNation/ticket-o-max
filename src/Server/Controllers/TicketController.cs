using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.Helpers;
using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.Server.Services;
using Microsoft.AspNetCore.Mvc;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using AlfaBureau.TicketOMax.Server.Helpers.Enums;

namespace AlfaBureau.TicketOMax.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TicketController : ControllerBase
    {
        private UserViewEntity CurrentUser => (UserViewEntity)HttpContext.Items["User"];

        private readonly ITicketService TicketService;

        public TicketController(ITicketService ticketService)
        {
            TicketService = ticketService;
        }

        /// <inheritdoc cref="ITicketService.GetAllSupports" />
        [HttpGet]
        [Route("AllSupports")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetAllSupports()
        {
            var allSupports = TicketService.GetAllSupports();
            return Ok(allSupports.Select(x => new KeyValuePair<int,string>(x.Id_Support, x.Label)));
        }

        /// <inheritdoc cref="ITicketService.GetAllCategories" />
        [HttpGet]
        [Route("AllCategories")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetAllCategories()
        {
            var allSupports = TicketService.GetAllCategories();
            return Ok(allSupports.Select(x => new KeyValuePair<int,string>(x.Id_Category, x.Label)));
        }

        /// <inheritdoc cref="ITicketService.GetAllStatus" />
        [HttpGet]
        [Route("AllStatus")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetAllStatus()
        {
            var allStatus = TicketService.GetAllStatus();
            return Ok(allStatus.Select(x => new KeyValuePair<int,string>(x.Id_Status, x.Label)));
        }

        /// <inheritdoc cref="ITicketService.GetAllTickets" />
        [HttpGet]
        [Route("AllTickets")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetAllTickets()
        {
            var allTickets = TicketService.GetAllTickets();

            var res = allTickets.Select(x => new
            {
                IdTicket = x.Id_Ticket,
                x.Code,
                x.Title,
                x.Description,
                IssuedBy = $"{x.Firstname_User_Issue} {x.Lastname_User_Issue}",
                IssueDate = x.Date_Issue,
                AssignedTo = string.IsNullOrEmpty(x.Firstname_User_Assignee) ? null : $"{x.Firstname_User_Assignee} {x.Lastname_User_Assignee}",
                ResolvedBy = string.IsNullOrEmpty(x.Firstname_User_Resolve) ? null : $"{x.Firstname_User_Resolve} {x.Lastname_User_Resolve}",
                ResolveDate = x.Date_Resolve,
                Category = x.Label_Category,
                State = x.Label_Status,
                IdState = x.Id_Status
            });

            return Ok(res);
        }

        /// <inheritdoc cref="ITicketService.GetTicket(int)" />
        [HttpGet]
        [Route("Ticket/{id}")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetTicket(int id)
        {
            var entity = TicketService.GetTicket(id);

            if(entity == null)
                return NotFound();

            var res = new
            {
                IdTicket = entity.Id_Ticket,
                entity.Code,
                entity.Title,
                entity.Description,
                IssuedBy = $"{entity.Firstname_User_Issue} {entity.Lastname_User_Issue}",
                IssueDate = entity.Date_Issue,
                AssignedTo = string.IsNullOrEmpty(entity.Firstname_User_Assignee) ? null : $"{entity.Firstname_User_Assignee} {entity.Lastname_User_Assignee}",
                IdAssignee = entity.Id_User_Assignee,
                ResolvedBy = string.IsNullOrEmpty(entity.Firstname_User_Resolve) ? null : $"{entity.Firstname_User_Resolve} {entity.Lastname_User_Resolve}",
                IdResolver = entity.Id_User_Resolve,
                ResolveDate = entity.Date_Resolve,
                Category = entity.Label_Category,
                IdCategory = entity.Id_Category,
                State = entity.Label_Status,
                IdState = entity.Id_Status
            };

            return Ok(res);
        }

        /// <inheritdoc cref="ITicketService.CreateTicket(CreateTicketRequest, int)" />
        [HttpPost]
        [Route("NewTickets")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult PostTicket(CreateTicketRequest model)
        {
            return Ok(TicketService.CreateTicket(model, CurrentUser.Id_User));
        }
        
        /// <inheritdoc cref="ITicketService.CreateTicket(CreateTicketRequest, int)" />
        [HttpPut]
        [Route("Ticket/{id}")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult PutTicket(int id, [FromBody] UpdateTicketRequest model)
        {
            TicketService.UpdateTicket(id, model, CurrentUser.Id_User);
            return Ok();
        }

        /// <inheritdoc cref="ITicketService.DesactivateTicket(int, int)" />
        [HttpDelete]
        [Route("Ticket/{id}")]
        [Produces("application/json")]
        [Authorize(Privilege.Moderator)]
        public IActionResult DeleteTicket(int id)
        {
            TicketService.DesactivateTicket(id, CurrentUser.Id_User);
            return Ok();
        }
    }
}