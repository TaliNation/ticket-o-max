using System;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.Helpers;
using AlfaBureau.TicketOMax.Server.Helpers.Enums;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using AlfaBureau.TicketOMax.Server.Services;
using Microsoft.AspNetCore.Mvc;

namespace AlfaBureau.TicketOMax.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private UserViewEntity CurrentUser => (UserViewEntity)HttpContext.Items["User"];

        private readonly IUserService UserService;

        public UserController(IUserService userService)
        {
            UserService = userService;
        }

        /// <inheritdoc cref="IUserService.GetAllPrivileges" />
        [HttpGet]
        [Route("AllPrivileges")]
        [Produces("application/json")]
        [Authorize(Privilege.Administrator)]
        public IActionResult GetAllPrivileges()
        {
            return Ok(UserService.GetAllPrivileges());
        }

        /// <inheritdoc cref="IUserService.Register(RegisterRequest, int)"/>
        [HttpPost]
        [Route("Register")]
        [Produces("application/json")]
        [Authorize(Privilege.Administrator)]
        public IActionResult Register(RegisterRequest model)
        {
            UserService.CreateUser(model, CurrentUser.Id_User);
            return Ok();
        }

        /// <inheritdoc cref="IUserService.Authenticate(LoginRequest)" />
        [HttpPost]
        [Route("Login")]
        [Produces("application/json")]
        public IActionResult Login(LoginRequest model)
        {
            string res = UserService.Authenticate(model);
            return Ok(res);
        }

        /// <inheritdoc cref="IUserService.GetAllUsers" />
        [HttpGet]
        [Route("AllUsers")]
        [Produces("application/json")]
        [Authorize(Privilege.Administrator)]
        public IActionResult GetAllUsers()
        {
            var users = UserService.GetAllUsers();

            var res = users.Select(x => new
            {
                Id = x.Id_User,
                x.Lastname,
                x.Firstname,
                x.Email,
                Privilege = x.Label_Privilege
            });

            return Ok(res);
        }

        /// <summary>Récupération l'utilisateur avant fait la requête</summary>
        [HttpGet]
        [Route("CurrentUser")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetCurrentUser()
        {
            return Ok(CurrentUser);
        }

		/// <summary>Récupération des privilèges de l'utilisateur avant fait la requête</summary>
        [HttpGet]
        [Route("CurrentUserPrivileges")]
        [Produces("application/json")]
        [Authorize]
        public IActionResult GetCurrentUserPrivileges()
        {
            return Ok(CurrentUser.Label_Privilege);
        }

        /// <inheritdoc cref="IUserService.ChangeUserPrivilege(PrivilegeChangeRequest, int)" />
        [HttpPut]
        [Route("UserPrivilege")]
        [Produces("application/json")]
        [Authorize(Privilege.Administrator)]
        public IActionResult ChangeUserPrivilege(PrivilegeChangeRequest model)
        {
            UserService.ChangeUserPrivilege(model, CurrentUser.Id_User);

            return Ok();
        }
    }
}