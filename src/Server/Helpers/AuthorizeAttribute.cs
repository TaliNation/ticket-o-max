using System;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.Helpers.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using AlfaBureau.TicketOMax.Shared.Constants;

namespace AlfaBureau.TicketOMax.Server.Helpers
{
    /// <summary>Attribut permettant de vérifier que l'utilisateur est authentifié et qu'il a les droits d'accès sur l'API</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly Privilege MinimumPrivilegeRequired;

        public AuthorizeAttribute()
        {
            MinimumPrivilegeRequired = Privilege.User;
        }

        public AuthorizeAttribute(Privilege minimumPrivilegeRequired)
        {
            MinimumPrivilegeRequired = minimumPrivilegeRequired;
        }

        /// <summary>Vérification de l'identité et des droits d'accès</summary>
        /// <remarks>
        /// Cette fonction se lance juste avant qu'une API avec l'attribut <see cref="AuthorizeAttribute"/> s'exécute.
        /// Si l'utilisateur n'est pas authorisé à y accéder, elle retourne une erreur 401 et l'API ne s'exécute pas.
        /// </remarks>
        /// <param name="context">Données à l'API par l'utilisateur</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (UserViewEntity)context.HttpContext.Items["User"];

            if (user == null || IsLackingPrivileges(user.Label_Privilege))
            {
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }

        private bool IsLackingPrivileges(string userPrivilege)
        {
            if(userPrivilege == PrivilegeConstants.User && (MinimumPrivilegeRequired == Privilege.User))
                return false;
            else if(userPrivilege == PrivilegeConstants.Moderator && (MinimumPrivilegeRequired == Privilege.Moderator || MinimumPrivilegeRequired == Privilege.User))
                return false;
            else if(userPrivilege == PrivilegeConstants.Administrator)
                return false;
            else
                return true;
        }
    }
}