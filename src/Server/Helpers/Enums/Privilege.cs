namespace AlfaBureau.TicketOMax.Server.Helpers.Enums
{
    public enum Privilege
    {
        User,
        Moderator,
        Administrator
    }
}