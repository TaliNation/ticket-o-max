namespace AlfaBureau.TicketOMax.Server.Helpers.Configurations
{
    /// <summary>Paramètres du fichier AppSettings pour l'authentification, section "JwtSettings"</summary>
    public class JwtSettings
    {
        /// <summary>Clef de génération des JSON Web Token</summary>
        public string Secret { get; set; }
    }
}