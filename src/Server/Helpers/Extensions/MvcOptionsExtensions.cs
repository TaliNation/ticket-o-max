using AlfaBureau.TicketOMax.Server.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace AlfaBureau.TicketOMax.Server.Helpers.Extensions
{
    public static class MvcOptionsExtensions
    {
        /// <summary>Ajout d'un pefixe qui précedera toutes les routes vers les API</summary>
        public static void UseGeneralRoutePrefix(this MvcOptions value, IRouteTemplateProvider routeAttribute) =>
            value.Conventions.Add(new RoutePrefixConvention(routeAttribute));

        /// <summary>Ajout d'un pefixe qui précedera toutes les routes vers les API</summary>
        /// <param name="value"></param>
        /// <param name="prefix">Ne doit pas terminer par "/"</param>
        public static void UseGeneralRoutePrefix(this MvcOptions value, string prefix)
        {
            prefix = RemoveEndSlash(prefix);
            value.UseGeneralRoutePrefix(new RouteAttribute(prefix));
        }

        private static string RemoveEndSlash(string s) =>
            s.EndsWith('/') ? s[0..^2] : s;
    }
}