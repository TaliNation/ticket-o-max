using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Server.Helpers.Configurations;
using AlfaBureau.TicketOMax.Server.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace AlfaBureau.TicketOMax.Server.Helpers.Middlewares
{
    /// <summary>Middlewares d'authentification par JSON Web Token</summary>
    /// <seealso href="https://fr.wikipedia.org/wiki/Middleware" />
    /// <seealso href="https://fr.wikipedia.org/wiki/JSON_Web_Token"/>
    public class JwtMiddleware
    {
        private readonly RequestDelegate Next;

        private readonly JwtSettings JwtSecret;

        public JwtMiddleware(RequestDelegate next, IOptions<JwtSettings> jwtSecret)
        {
            Next = next;
            JwtSecret = jwtSecret.Value;
        }

        /// <summary>Récupération de l'utilisateur lorsqu'une requête HTTP arrive sur le serveur</summary>
        /// <remarks>Le préfixe Async n'est pas renseigné car le nom des méthodes dans un middleware est normalisé et ne peut pas être changé</remarks>
        public async Task Invoke(HttpContext httpContext, IUserService userService)
        {
            var token = httpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if(token != null)
                AttachUserToContext(httpContext, userService, token);

            await Next(httpContext);
        }

        private void AttachUserToContext(HttpContext httpContext, IUserService userService, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(JwtSecret.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

                httpContext.Items["User"] = userService.GetUserWithPrivilegeById(userId);
            }
            catch { }
        }
    }
}