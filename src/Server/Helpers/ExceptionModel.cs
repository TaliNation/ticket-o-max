using System;

namespace AlfaBureau.TicketOMax.Server.Helpers
{
    /// <summary>Modele de formatage des exceptions dans les réponses HTTP</summary>
    public class ExceptionModel
    {
        public string Message { get; }
        public string StackTrace { get; }

        /// <summary>Modèle de formatage de la sous-exception</summary>
        public ExceptionModel InnerException { get; }

        public ExceptionModel(Exception e)
        {
            if(e == null)
                return;

            Message = e.Message;
            StackTrace = e.StackTrace;
            InnerException = new ExceptionModel(e.InnerException);
        }
    }
}