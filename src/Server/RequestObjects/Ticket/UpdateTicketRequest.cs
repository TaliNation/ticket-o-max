using System;
using System.ComponentModel.DataAnnotations;

namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <inheritdoc cref="IRequest" />
    public class UpdateTicketRequest : IRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int IdCategory { get; set; }
        public int? IdAssignee { get; set; }
        public int? IdResolver { get; set; }
        public int IdState { get; set; }
        public DateTime? ResolveDate { get; set; }

        /// <inheritdoc />
        public bool IsValid
        {
            get
            {
                bool res = true;

                res &= !string.IsNullOrEmpty(Title) && Title.Length < 256;
                res &= !string.IsNullOrEmpty(Description) && Description.Length <= 8000;
                res &= IdCategory != 0;
                res &= IdState != 0;

                return res;
            }
        }
    }
}