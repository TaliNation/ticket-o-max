namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <summary>Objets obtenu à partir du corps d'une requête HTTP, qui est ensuite passé en paramètre aux services</summary>
    public interface IRequest
    {
        /// <summary>Validité des données de l'objet</summary>
        /// <example>
        /// Est-ce que la donnée n'est pas <see langword="null"/>, pas vide, comprise dans une certaine plage...
        /// </example>
        bool IsValid { get; }
    }
}