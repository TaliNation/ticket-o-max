namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <inheritdoc cref="IRequest" />
    public class LoginRequest : IRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }

        /// <inheritdoc />
        public bool IsValid
        {
            get
            {
                bool res = true;

                res &= !string.IsNullOrEmpty(Email) && Email.Length < 256;
                res &= !string.IsNullOrEmpty(Password) && Password.Length < 256;

                return res;
            }
        }
    }
}