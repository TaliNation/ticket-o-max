using System.ComponentModel.DataAnnotations;

namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <inheritdoc />
    public class RegisterRequest : IRequest
    {
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsModerator { get; set; }

        /// <inheritdoc />
        /// <remarks>
        /// L'adresse email doit correspondre au standard : x@x.x
        /// </remarks>
        public bool IsValid
        {
            get
            {
                bool res = true;

                res &= !string.IsNullOrEmpty(Lastname) && Lastname.Length < 256;
                res &= !string.IsNullOrEmpty(Firstname) && Firstname.Length < 256;
                res &= !string.IsNullOrEmpty(Email) && Email.Length < 256;
                res &= new EmailAddressAttribute().IsValid(Email);
                res &= !string.IsNullOrEmpty(Password);

                return res;
            }
        }
    }
}
