namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <inheritdoc cref="IRequest" />
    public class PrivilegeChangeRequest : IRequest
    {
        public int UserId { get; set; }
        public bool IsPromotion { get; set; }

        /// <inheritdoc />
        /// <remarks>
        /// Aucune implémentation les types fort <see langword="int" /> et <see langword="bool"/> sont suffisants pour valider les données
        /// </remarks>
        public bool IsValid => true;
    }
}