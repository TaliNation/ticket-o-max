using System.Data;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.Server.Helpers.Configurations;
using AlfaBureau.TicketOMax.Server.Helpers.Middlewares;
using AlfaBureau.TicketOMax.Server.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Text.Json;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using AlfaBureau.TicketOMax.Server.Helpers;
using Microsoft.AspNetCore.Http;

namespace AlfaBureau.TicketOMax.Server
{
    /// <summary>Configuration au démarrage du serveur</summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>Ajout et configuration des services</summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDbContext>(_ => new DbContext(new SqlConnection(Configuration.GetConnectionString("TicketOMax"))));

            services.Configure<JwtSettings>(Configuration.GetSection("JwtSettings"));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITicketService, TicketService>();

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AlfaBureau.TicketOMax.Server", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "JSON Web Token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        System.Array.Empty<string>()
                    }
                });
            });
        }

        /// <summary>Configuration générale</summary>
        /// <param name="app">Propriétés de l'application</param>
        /// <param name="env">Environnement d'exécution de l'application (développement, production...)</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "AlfaBureau.TicketOMax.Server v1");
                    c.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(endpoints => endpoints.MapControllers());

            app.UseExceptionHandler(
                builder =>
                {
                    builder.Run(
                        async context =>
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.ContentType = "application/json";
                            var exception = context.Features.Get<IExceptionHandlerFeature>();

                            if(exception != null)
                            {
                                var error = new ExceptionModel(exception.Error);
                                var errorObject = JsonSerializer.Serialize(error);
                                await context.Response.WriteAsync(errorObject).ConfigureAwait(false);
                            }
                        }
                    );
                }
            );
        }
    }
}
