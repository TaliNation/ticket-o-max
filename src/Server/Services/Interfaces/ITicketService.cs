using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using AlfaBureau.TicketOMax.Server.Services.Core;

namespace AlfaBureau.TicketOMax.Server.Services
{
    /// <summary><see href="https://en.wikipedia.org/wiki/Service_layer_pattern">Service</see> lié aux tickets</summary>
    public interface ITicketService : IService
    {
        List<TicketViewEntity> GetAllTickets();
        List<SupportEntity> GetAllSupports();
        List<StatusEntity> GetAllStatus();
        List<CategoryEntity> GetAllCategories();
        int CreateTicket(CreateTicketRequest model, int currentUserId);
        void UpdateTicket(int idTicket, UpdateTicketRequest model, int currentUserId);
        TicketViewEntity GetTicket(int id);
        void DesactivateTicket(int id, int currentUserId);
    }
}
