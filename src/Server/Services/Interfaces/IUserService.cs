using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using AlfaBureau.TicketOMax.Server.Services.Core;

namespace AlfaBureau.TicketOMax.Server.Services
{
    /// <summary><see href="https://en.wikipedia.org/wiki/Service_layer_pattern">Service</see> lié aux utilisateurs</summary>
    public interface IUserService : IService
    {
        /// <summary>Vérification des données de connexion des utilisateurs et génération d'un JSON Web Token</summary>
        /// <returns>JSON Web Token</returns>
        /// <seealso cref="IRequest"/>
        string Authenticate(LoginRequest loginRequest);

        /// <summary>Création d'un nouvel utilisateur dans la base de donnée et authentification</summary>
        /// <param name="registerRequest"></param>
        /// <param name="currentUserId">ID de l'utilisateur qui a émis la requête (via contexte HTTP)</param>
        /// <returns>JSON Web Token</returns>
        /// <seealso cref="IRequest" />
        /// <seealso cref="Authenticate(LoginRequest)" />
        string Register(RegisterRequest registerRequest, int currentUserId);

        void CreateUser(RegisterRequest model, int currentUserId);

        List<PrivilegeEntity> GetAllPrivileges();
        UserEntity GetUserById(int id);
        List<UserViewEntity> GetAllUsers();

        /// <summary>Promotion d'un utilisateur en modérateur et inversement</summary>
        /// <param name="model"></param>
        /// <param name="currentUserId">ID de l'utilisateur qui a émis la requête (via contexte HTTP)</param>
        public void ChangeUserPrivilege(PrivilegeChangeRequest model, int currentUserId);

        UserViewEntity GetUserWithPrivilegeById(int id);
    }
}