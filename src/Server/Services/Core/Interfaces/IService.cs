namespace AlfaBureau.TicketOMax.Server.Services.Core
{
    /// <summary>Classes permettant d'organiser les services par domaines</summary>
    /// <remarks>
    /// Les services permettent de bien organiser les fonctionnalités au sein du code source pour faciliter la lisibilité, mais aussi à pouvoir les tester unitairement car ils ne retournent pas directement de réponses HTTP
    /// </remarks>
    public interface IService { }
}