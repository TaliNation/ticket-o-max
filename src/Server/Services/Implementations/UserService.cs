using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using System;
using AlfaBureau.TicketOMax.Server.Models;
using AlfaBureau.TicketOMax.Server.Helpers.Configurations;
using Microsoft.Extensions.Options;
using AlfaBureau.TicketOMax.Shared.Constants;

namespace AlfaBureau.TicketOMax.Server.Services
{
    /// <inheritdoc />
    public class UserService : IUserService
    {
        private readonly IDbContext DbContext;

        /// <summary>Configuration contenant le secret</summary>
        private readonly JwtSettings JwtSettings;

        public UserService(IOptions<JwtSettings> jwtSettings, IDbContext dbContext)
        {
            JwtSettings = jwtSettings.Value;
            DbContext = dbContext;
        }

        /// <inheritdoc />
        public string Authenticate(LoginRequest model)
        {
            if(model?.IsValid != true)
                throw new ArgumentException("Invalid data");

            var entity = DbContext.User.General.GetByColumn("EMAIL", model.Email.Trim().ToLower()).FirstOrDefault();

            if(entity == null || !BCrypt.Net.BCrypt.Verify(model.Password, entity.Password))
                throw new ArgumentException("Wrong email or password");

            var user = new User(entity);
            return user.GerenateJwtToken(JwtSettings);
        }

        /// <inheritdoc />
        public string Register(RegisterRequest model, int currentUserId)
        {
            CreateUser(model, currentUserId);

            var authenticationModel = new LoginRequest
            {
                Email = model.Email,
                Password = model.Password
            };

            return Authenticate(authenticationModel);
        }

        /// <inheritdoc />
        public void CreateUser(RegisterRequest model, int currentUserId)
        {
            if(model?.IsValid != true)
                throw new ArgumentException("Invalid data");

            var entity = DbContext.User.General.GetByColumn("EMAIL", model.Email).FirstOrDefault();

            if(entity != null)
                throw new ArgumentException($"User with email {model.Email} already exists");

            var newUser = new
            {
                LASTNAME = model.Lastname,
                FIRSTNAME = model.Firstname,
                PASSWORD = BCrypt.Net.BCrypt.HashPassword(model.Password),
                EMAIL = model.Email,
                ID_PRIVILEGE = GetPrivilegeId(model.IsModerator),
                ID_USER_CREATE = currentUserId
            };

            DbContext.User.Crud.Add(newUser);
        }

        /// <inheritdoc />
        public List<UserViewEntity> GetAllUsers()
        {
            var entities = DbContext.UserView.General.GetAll();
            return entities.ToList();
        }

        /// <inheritdoc />
        public void ChangeUserPrivilege(PrivilegeChangeRequest model, int currentUserId)
        {
            if(model?.IsValid != true)
                throw new ArgumentException("Invalid data");

            var userToChange = DbContext.User.Crud.GetById(model.UserId);

            if(userToChange == null)
                throw new ArgumentException("User to change not found");

            int privilegeId = GetPrivilegeId(model.IsPromotion);

            DbContext.User.Crud.Update(model.UserId, new
            {
                ID_PRIVILEGE = privilegeId,
                ID_USER_UPDATE = currentUserId
            });
        }

        /// <inheritdoc />
        public List<PrivilegeEntity> GetAllPrivileges()
        {
            var entities = DbContext.Privilege.General.GetAll();

            return entities.ToList();
        }

        private int GetPrivilegeId(bool isModerator)
        {
            var privilegeEntities = GetAllPrivileges();

            if (isModerator)
            {
                return privilegeEntities
                    .Find(x => string.Equals(x.Label, PrivilegeConstants.Moderator, StringComparison.OrdinalIgnoreCase))
                    .Id_Privilege;
            }
            else
            {
                return privilegeEntities
                    .Find(x => string.Equals(x.Label, PrivilegeConstants.User, StringComparison.OrdinalIgnoreCase))
                    .Id_Privilege;
            }
        }

        /// <inheritdoc />
        public UserEntity GetUserById(int id)
        {
            return DbContext.User.Crud.GetById(id);
        }

        public UserViewEntity GetUserWithPrivilegeById(int id)
        {
            return DbContext.UserView.General.GetByColumn("ID_USER", id).FirstOrDefault();
        }
    }
}