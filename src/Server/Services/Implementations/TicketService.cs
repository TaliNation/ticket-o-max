using System.Collections.Generic;
using System.Linq;
using AlfaBureau.TicketOMax.DataAccess;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using AlfaBureau.TicketOMax.Shared.Constants;
using System;

#pragma warning disable RCS1202

namespace AlfaBureau.TicketOMax.Server.Services
{
    /// <inheritdoc />
    public class TicketService : ITicketService
    {
        private readonly IDbContext DbContext;

        public TicketService(IDbContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <inheritdoc />
        public List<TicketViewEntity> GetAllTickets()
        {
            var entities = DbContext.TicketView.GetAllActivated();

            return entities.ToList();
        }

        /// <inheritdoc />
        public List<StatusEntity> GetAllStatus()
        {
            var entities = DbContext.Status.General.GetAll();

            return entities.ToList();
        }

        /// <inheritdoc />
        public List<SupportEntity> GetAllSupports()
        {
            var entities = DbContext.Support.General.GetAll();

            var res = entities.ToList();

            return res;
        }

        /// <inheritdoc />
        public List<CategoryEntity> GetAllCategories()
        {
            var entities = DbContext.Category.General.GetAll();

            var res = entities.ToList();

            return res;
        }

        /// <inheritdoc />
        public int CreateTicket(CreateTicketRequest model, int currentUserId)
        {
            if(model?.IsValid != true)
                throw new ArgumentException("Invalid data");

            var currentStatus = DbContext.Status.General.GetByColumn("LABEL", StatusConstants.Open);

            string code = GenerateUniqueCode();

            if(currentStatus == null)
                throw new Exception($"Status {StatusConstants.Open} does not exist!");

            var newTicket = new
            {
                CODE = code,
                TITLE = model.Title,
                DESCRIPTION = model.Description,
                ID_STATUS = currentStatus.FirstOrDefault().Id_Status,
                ID_CATEGORY = model.IdCategory,
                ID_USER_ISSUE = currentUserId,
                ID_USER_CREATE = currentUserId
            };

            return DbContext.Ticket.Crud.AddGetId(newTicket);
        }
        public void UpdateTicket(int idTicket, UpdateTicketRequest model, int currentUserId)
        {
            if(model?.IsValid != true)
                throw new ArgumentException("Invalid data");

            var updatedTicket = new
            {
                TITLE = model.Title,
                DESCRIPTION = model.Description,
                ID_STATUS = model.IdState,
                ID_CATEGORY = model.IdCategory,
                ID_USER_UPDATE = currentUserId,
                DATE_UPDATE = DateTime.Now,
                ID_USER_ASSIGNEE = model.IdAssignee,
                ID_USER_RESOLVE = model.IdResolver,
                DATE_RESOLVE = model.ResolveDate
            };

            DbContext.Ticket.Crud.Update(idTicket, updatedTicket);
        }


        private string GenerateUniqueCode()
        {
            string code;
            bool unique = false;
            do
            {
                code = GenerateRandomString(12);
                var codeDb = DbContext.Ticket.General.GetByColumn("CODE", code).Select(x => x.Code);
                if (!codeDb.Contains(code))
                {
                    unique = true;
                }
            }
            while (!unique);

            return code;
        }

        private static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();

            return new string(Enumerable
                .Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());
        }

        /// <inheritdoc />
        public TicketViewEntity GetTicket(int id)
        {
            return DbContext.TicketView.General.GetByColumn("ID_TICKET", id).SingleOrDefault();
        }

        /// <inheritdoc />
        public void DesactivateTicket(int id, int currentUserId)
        {
            DbContext.Ticket.Crud.Update(id, new
            {
                ID_USER_DELETE = currentUserId,
                DATE_DELETE = DateTime.Now
            });
        }
    }
}
