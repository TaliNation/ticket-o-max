using System;
using System.Net.Http;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers;
using AlfaBureau.TicketOMax.Client.Helpers.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using Radzen;

namespace AlfaBureau.TicketOMax.Client
{
    public static class Program
    {
        /// <summary>Configuration et lancement du serveur</summary>
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddBlazoredLocalStorage();
            builder.Services.AddAuthorizationCore();
            builder.Services.AddScoped<DialogService>();

            string apiRootUrl = builder.Configuration["ApiRootUrl"];

            builder.Services.AddScoped(_ => new HttpClient { BaseAddress = new Uri(apiRootUrl) });

            builder.Services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
            builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();

            await builder.Build().RunAsync();
        }
    }
}
