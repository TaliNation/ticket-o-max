using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Enums;
using AlfaBureau.TicketOMax.Client.RequestObjects;
using AlfaBureau.TicketOMax.Client.ResponseObjects;
using Microsoft.AspNetCore.Components;

namespace AlfaBureau.TicketOMax.Client.Pages.User
{
    public class UserListBase : ComponentBase
    {
        [Inject] private HttpClient Http { get; init; }

        protected List<UserResponse> Users { get; set; } = new();

        /// <inheritdoc cref="AlfaBureau.TicketOMax.Client.Helpers.Enums.ContentStatus"/>
        protected ContentStatus ContentStatus { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await GetUsersAsync();

            ContentStatus = ContentStatus.Ok;
        }

        /// <summary>Changement des privileges de l'utilisateur</summary>
        /// <remarks>
        /// Le paramètre userId est définie en fonction de la ligne dans le tableau.
        /// Le paramètre isPromotion est définie en fonction des privileges actuels de l'utilisateur
        /// </remarks>
        /// <param name="userId"></param>
        /// <param name="isPromotion">Est-ce que l'utilisateur passe modérateur</param>
        protected async Task ChangeUserPrivilegeAsync(int userId, bool isPromotion)
        {
            ContentStatus = ContentStatus.Loading;

            await Http.PutAsJsonAsync<PrivilegeChangeRequest>("User/UserPrivilege", new()
            {
                UserId = userId,
                IsPromotion = isPromotion
            });

            await GetUsersAsync();

            ContentStatus = ContentStatus.Ok;
        }

        /// <summary>Récupération des données de la liste d'utilisateurs dans la base de données</summary>
        private async Task GetUsersAsync()
        {
            var allUsersHttpResponse = await Http.GetAsync("User/AllUsers");
            Users = await allUsersHttpResponse.Content.ReadAsAsync<List<UserResponse>>();
        }
    }
}