using System.Net.Http;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Services;
using AlfaBureau.TicketOMax.Client.RequestObjects;
using Microsoft.AspNetCore.Components;

namespace AlfaBureau.TicketOMax.Client.Pages.User
{
    /// <summary>Code client de NewUser</summary>
    public class NewUserBase : ComponentBase
    {
        [Inject] private HttpClient Http { get; init; }
        [Inject] private NavigationManager NavigationManager { get; init; }

        /// <summary>Données du formulaire à envoyer à l'API</summary>
        protected AuthenticationRequest RegisterData { get; set; } = new();

        /// <summary>Champ de confirmation du mot de passe</summary>
        protected string PasswordConfirmation { get; set; } = "";

        /// <summary>Message d'information</summary>
        protected string Message { get; set; } = "";

        /// <summary>Envoi des données d'inscription au serveur</summary>
        protected async Task RegisterAsync()
        {
            if(RegisterData.Password != PasswordConfirmation)
            {
                Message = "Les mots de passe ne correspondent pas";
                return;
            }

            var httpResponse = await Http.PostAsJsonAsync("User/Register", RegisterData);

            if(!httpResponse.IsSuccessStatusCode)
            {
                Message = "Les données sont invalides ou l'utilisateur existe déjà";
            }
            else
            {
                NavigationManager.NavigateTo("/users");
            }
        }
    }
}