using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Enums;
using AlfaBureau.TicketOMax.Client.ResponseObjects;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using Microsoft.AspNetCore.Components;

namespace AlfaBureau.TicketOMax.Client.Pages.Ticket
{
    /// <summary>Code client de TicketForm</summary>
    public class EditTicketBase : ComponentBase
    {
        [Parameter] public int Id { get; init; }

        [Inject] private HttpClient Http { get; init; }
        [Inject] private NavigationManager NavigationManager { get; init; }

        protected TicketResponse Ticket { get; set; }
        protected ContentStatus ContentStatus { get; set; }
        protected List<KeyValuePair<int, string>> Categories { get; set;} = new();
        protected List<KeyValuePair<int, string>> States { get; set;} = new();
        protected List<UserResponse> Users { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            Categories = await Http.GetFromJsonAsync<List<KeyValuePair<int, string>>>("Ticket/AllCategories");
            States = await Http.GetFromJsonAsync<List<KeyValuePair<int, string>>>("Ticket/AllStatus");

            var allUsersHttpResponse = await Http.GetAsync("User/AllUsers");
            Users = await allUsersHttpResponse.Content.ReadAsAsync<List<UserResponse>>();

            var httpResponse = await Http.GetAsync($"Ticket/Ticket/{Id}");

            if(httpResponse.StatusCode == HttpStatusCode.NotFound)
            {
                ContentStatus = ContentStatus.Error;
                return;
            }

            Ticket = await httpResponse.Content.ReadAsAsync<TicketResponse>();

            ContentStatus = ContentStatus.Ok;
        }

        protected async void SubmitAsync() 
        {
            if (Ticket.IdResolver != null)
            {
                Ticket.ResolveDate = DateTime.Now;
            }
            else
            {
                Ticket.ResolveDate = null;
            }
            
            await Http.PutAsJsonAsync($"Ticket/Ticket/{Id}", Ticket);
            NavigationManager.NavigateTo($"/Ticket/{Id}", true);
        }
    }
}
