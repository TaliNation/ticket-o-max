using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Enums;
using AlfaBureau.TicketOMax.Client.ResponseObjects;
using Microsoft.AspNetCore.Components;
using Radzen;

namespace AlfaBureau.TicketOMax.Client.Pages.Ticket
{
    /// <summary>Code client de TicketForm</summary>
    public class TicketListBase : ComponentBase
    {
        [Inject] private HttpClient Http { get; init; }
        [Inject] protected DialogService DialogService { get; init; }

		protected string CurrentUserPrivilege { get; set; }
        protected List<TicketResponse> Tickets { get; set; } = new();

        /// <inheritdoc cref="AlfaBureau.TicketOMax.Client.Helpers.Enums.ContentStatus" />
        protected ContentStatus ContentStatus { get; set; }
        protected string Message { get; set; } = "";

        protected override async Task OnInitializedAsync()
        {
            DialogService.OnClose += CloseAsync;
            DialogService.OnOpen += Open;
            await InitTicketTabAsync();

			var httpResponse = await Http.GetAsync("User/CurrentUserPrivileges");
			CurrentUserPrivilege = await httpResponse.Content.ReadAsAsync<string>();

            ContentStatus = ContentStatus.Ok;
        }

        private async Task InitTicketTabAsync()
        {
            var httpResponse = await Http.GetAsync("Ticket/AllTickets");

            if(!httpResponse.IsSuccessStatusCode)
            {
                Message = "Une erreur est survenue lors de la récupération des tickets.";
                return;
            }

            Tickets = await httpResponse.Content.ReadAsAsync<List<TicketResponse>>();
        }

        private async void DeleteTicketAsync(int IdTicket)
        {
            await Http.DeleteAsync($"Ticket/Ticket/{IdTicket}");
        }

        protected async Task ShowConfirmDialogAsync(int id)
        {
			DeleteTicketAsync(id);
			await InitTicketTabAsync();
			StateHasChanged();
        }

        private void Open(string title, Type type, Dictionary<string, object> parameters, DialogOptions options)
        {}

        private async void CloseAsync(dynamic result)
        {
            if (result.ToString() == "insert true")
            {
                await InitTicketTabAsync();
                StateHasChanged();
            }
            else if ((int)result > 0)
            {
                DeleteTicketAsync(result);
                await InitTicketTabAsync();
                StateHasChanged();
            }
        }
    }
}
