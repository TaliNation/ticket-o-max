using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Enums;
using AlfaBureau.TicketOMax.Client.ResponseObjects;
using AlfaBureau.TicketOMax.Shared.Constants;
using Microsoft.AspNetCore.Components;

namespace AlfaBureau.TicketOMax.Client.Pages.Ticket
{
    public class ViewTicketBase : ComponentBase
    {
        [Inject] private HttpClient Http { get; init; }
        [Inject] private NavigationManager NavigationManager { get; init; }

        [Parameter] public int Id { get; init; }

        /// <summary>Informations du ticket</summary>
        protected TicketResponse Ticket { get; set; }

		protected string CurrentUserPrivilege { get; set; }

        /// <inheritdoc cref="AlfaBureau.TicketOMax.Client.Helpers.Enums.ContentStatus" />
        protected ContentStatus ContentStatus { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var httpResponse = await Http.GetAsync($"Ticket/Ticket/{Id}");

            if(httpResponse.StatusCode == HttpStatusCode.NotFound)
            {
                ContentStatus = ContentStatus.Error;
                return;
            }

            Ticket = await httpResponse.Content.ReadAsAsync<TicketResponse>();

			httpResponse = await Http.GetAsync("User/CurrentUserPrivileges");
			CurrentUserPrivilege = await httpResponse.Content.ReadAsAsync<string>();

            ContentStatus = ContentStatus.Ok;
        }

        private async void DeleteTicketAsync(int idTicket)
        {
            await Http.DeleteAsync($"Ticket/Ticket/{idTicket}");
        }

        protected void ShowConfirmDialog(int id)
        {
			DeleteTicketAsync(id);
            NavigationManager.NavigateTo("/", true);
        }
    }
}