using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.Helpers.Services;
using AlfaBureau.TicketOMax.Client.RequestObjects;
using Microsoft.AspNetCore.Components;
using System.Windows.Input;
using Microsoft.AspNetCore.Components.Web;

namespace AlfaBureau.TicketOMax.Client.Pages
{
    /// <summary>Code client de Login</summary>
    public class LoginBase : ComponentBase
    {
        [Inject] private IAuthenticationService AuthenticationService { get; init; }
        [Inject] private NavigationManager NavigationManager { get; init; }

        /// <summary>Données du formulaire à envoyer à l'API</summary>
        protected AuthenticationRequest LoginData { get; set; } = new();

        /// <summary>Message d'information</summary>
        protected string Message { get; set; } = "";

        /// <summary>Envoi des données de connexion au serveur</summary>
        protected async Task HandleLogin()
        {
            var jwt = await AuthenticationService.Login(LoginData);

            if (jwt != null)
                NavigationManager.NavigateTo("/", true);
            else
                Message = "Adresse email ou mot de passe incorrect";
        }

        /// <summary>Envoi des données de connexion au serveur en appuyant sur Entrée</summary>
        protected async Task SummitWithKeyboard(KeyboardEventArgs args)
        {
            if (args.Code == "Enter" || args.Code == "NumpadEnter")
            {
                await HandleLogin();
            }
        }
    }
}
