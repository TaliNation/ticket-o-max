using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.RequestObjects;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;

namespace AlfaBureau.TicketOMax.Client.Helpers.Services
{
    /// <inheritdoc cref="IAuthenticationService" />
    public class AuthenticationService : IAuthenticationService
    {
        private readonly HttpClient HttpClient;
        private readonly AuthenticationStateProvider AuthenticationStateProvider;
        private readonly ILocalStorageService LocalStorage;

        public AuthenticationService(HttpClient httpClient, AuthenticationStateProvider authenticationStateProvider, ILocalStorageService localStorage)
        {
            HttpClient = httpClient;
            AuthenticationStateProvider = authenticationStateProvider;
            LocalStorage = localStorage;
        }

        /// <inheritdoc />
        public async Task<string> Login(AuthenticationRequest request)
        {
            var httpResponse = await HttpClient.PostAsJsonAsync("User/Login", request);

            if (!httpResponse.IsSuccessStatusCode)
            {
                return null;
            }

            var loginResponse = await httpResponse.Content.ReadAsStringAsync();

            await StoreToken(loginResponse);

            return loginResponse;
        }

        /// <inheritdoc />
        public async Task Logout()
        {
            await LocalStorage.RemoveItemAsync("token");
            ((ApiAuthenticationStateProvider)AuthenticationStateProvider).MarkUserAsLoggedOut();
            HttpClient.DefaultRequestHeaders.Authorization = null;
        }

        private async Task StoreToken(string token)
        {
            await LocalStorage.SetItemAsync("token", token);
            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
        }
    }
}