using System.Threading.Tasks;
using AlfaBureau.TicketOMax.Client.RequestObjects;

namespace AlfaBureau.TicketOMax.Client.Helpers.Services
{
    /// <summary>Service d'authentification</summary>
    public interface IAuthenticationService
    {
        /// <summary>Authentication d'un utilisateur</summary>
        /// <returns>JSON Web Token</returns>
        Task<string> Login(AuthenticationRequest request);

        /// <summary>Déconnexion d'un utilisateur</summary>
        Task Logout();
    }
}