namespace AlfaBureau.TicketOMax.Client.Helpers.Enums
{
    /// <summary>Contrôle le niveau de chargement d'une ressource afin d'adapter l'affichage de la page</summary>
    public enum ContentStatus
    {
        Loading = 0,
        Ok = 1,
        Error = 2
    }
}
