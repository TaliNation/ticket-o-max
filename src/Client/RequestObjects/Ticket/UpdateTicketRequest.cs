using System;
using System.ComponentModel.DataAnnotations;

namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    public class UpdateTicketRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int IdCategory { get; set; }
        public int IdAssignee { get; set; }
        public int IdResolver { get; set; }
        public int IdState { get; set; }
        public DateTime DateResolve { get; set; }
    }
}