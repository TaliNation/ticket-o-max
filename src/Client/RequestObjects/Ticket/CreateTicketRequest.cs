using System.ComponentModel.DataAnnotations;

namespace AlfaBureau.TicketOMax.Server.RequestObjects
{
    /// <summary>Informations à envoyer à l'API pour créer un nouveau ticket</summary>
    public class CreateTicketRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int IdCategory { get; set; }
    }
}