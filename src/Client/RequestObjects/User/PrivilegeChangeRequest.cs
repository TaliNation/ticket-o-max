namespace AlfaBureau.TicketOMax.Client.RequestObjects
{
    /// <summary>Informations à envoyer à l'API pour changer les privileges d'un utilisateur
    /// </summary>
    public class PrivilegeChangeRequest
    {
        public int UserId { get; set; }
        public bool IsPromotion { get; set; }
    }
}