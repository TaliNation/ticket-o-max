namespace AlfaBureau.TicketOMax.Client.RequestObjects
{
    /// <summary>Informations à envoyer à l'API pour créer et authentifier un utilisateur</summary>
    /// <remarks>
    /// Le formulaire de connexion n'utilise que <see cref="Email"/> et <see cref="Password"/>
    /// </remarks>
    public class AuthenticationRequest
    {
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsModerator { get; set; }
    }
}