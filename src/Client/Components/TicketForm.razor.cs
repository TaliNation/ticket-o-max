using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using AlfaBureau.TicketOMax.Server.RequestObjects;
using System;

namespace AlfaBureau.TicketOMax.Client.Components
{
    /// <summary>Code client de TicketForm</summary>
    public class TicketFormBase : ComponentBase
    {
        [Inject] protected HttpClient Http { get; init;}
        [Inject] protected Radzen.DialogService DialogService { get; init; }
        protected List<KeyValuePair<int, string>> Categories { get; set;} = new();
        protected CreateTicketRequest NewTicketData { get; set; } = new();

        /// <summary>Message d'information</summary>
        protected string Message { get; set; } = "";
        protected override async Task OnInitializedAsync()
        {
            Categories = await Http.GetFromJsonAsync<List<KeyValuePair<int, string>>>("Ticket/AllCategories");
        }
        protected async Task SubmitAsync()
        {
            var httpResponse = await Http.PostAsJsonAsync("Ticket/NewTickets", NewTicketData);
            if (!httpResponse.IsSuccessStatusCode)
            {
                Message = "Erreur lors de la création, vérifiez que toutes les données sont valides";
                return;
            }

            var idTicket = await httpResponse.Content.ReadAsAsync<int>();
            if (idTicket != 0)
            {
                DialogService.Close("insert true");
            }
        }
    }
}
