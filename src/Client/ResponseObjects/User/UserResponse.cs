namespace AlfaBureau.TicketOMax.Client.ResponseObjects
{
    /// <summary>Informations sur les utilisateurs basées sur l'API User/AllUsers</summary>
    public class UserResponse
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }
        public string Privilege { get; set; }
        public bool IsActive { get; set; }

        /// <summary>Combinaison du prénom et du nom de l'utilisateur</summary>
        /// <example>
        /// Jean Valjean
        /// </example>
        public string FirstnameLastname => $"{Firstname} {Lastname}";
    }
}