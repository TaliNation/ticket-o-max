using System;

namespace AlfaBureau.TicketOMax.Client.ResponseObjects
{
    /// <summary>Informations récupérées de l'API pour la visualisation des tickets</summary>
    public class TicketResponse
    {
        public int IdTicket { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public int IdCategory { get; set; }
        public string Description { get; set; }
        public string IssuedBy { get; set; }
        public DateTime IssueDate { get; set; }
        public string AssignedTo { get; set; }
        public int? IdAssignee { get; set; }
        public string State { get; set; }
        public int IdState { get; set; }
        public string ResolvedBy { get; set; }
        public int? IdResolver { get; set; }
        public DateTime? ResolveDate { get; set; }
        public string ResolveDateAsString => ResolveDate.HasValue ? ResolveDate.Value.ToString("dd/MM/yyyy") : ""; 
        public string ResolveTimeAsString => ResolveDate.HasValue ? ResolveDate.Value.ToString("HH:mm") : "";
    }
}