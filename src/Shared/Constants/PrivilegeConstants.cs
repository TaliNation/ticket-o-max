namespace AlfaBureau.TicketOMax.Shared.Constants
{
    /// <summary>Constantes liés aux privilèges (rôles) des utilisateurs</summary>
    /// <remarks>
    /// Les valeurs correspondent à celles dans la base de données
    /// </remarks>
    public static class PrivilegeConstants
    {
        public const string User = "Utilisateur";
        public const string Moderator = "Modérateur";
        public const string Administrator = "Administrateur";
    }
}