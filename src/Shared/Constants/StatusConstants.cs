namespace AlfaBureau.TicketOMax.Shared.Constants
{
    /// <summary>Constantes liés aux statuts des tickets</summary>
    /// <remarks>
    /// Les valeurs correspondent à celles dans la base de données
    /// </remarks>
    public static class StatusConstants
    {
        public const string Open = "Ouvert";
        public const string Ongoing = "En cours";
        public const string Resolved = "Cloturé";
        public const string Closed = "Refusé";
    }
}