using System;
using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories.Core
{
    /// <inheritdoc cref="IRepository{TEntity}" />
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        private readonly string TableName;

        private QueryFactory QueryFactory { get; }

        public Repository(QueryFactory queryFactory, string tableName)
        {
            QueryFactory = queryFactory;
            TableName = tableName;
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetAll()
        {
            return QueryFactory.Query(TableName).Get<TEntity>();
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetByColumn(string columnName, IComparable value)
        {
            return QueryFactory.Query(TableName).Where($"{TableName}.{columnName}", "=", value).Get<TEntity>();
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetByColumn(Dictionary<string, IComparable> columnValueDictionary)
        {
            var query = QueryFactory.Query(TableName);
            foreach (var columnValuePair in columnValueDictionary)
                query = query.Where($"{TableName}.{columnValuePair.Key}", "=", columnValuePair.Value);

            return query.Get<TEntity>();
        }
    }
}