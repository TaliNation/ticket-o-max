using System;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories.Core
{
    /// <summary>Repository Create/Read/Update/Delete pour les tables avec une seule clef primaire.</summary>
    /// <typeparam name="TEntity" /><typeparam name="TPrimaryKey" />
    public class SingleIdCrudRepository<TEntity, TPrimaryKey> : ICrudRepository<TEntity, TPrimaryKey>
        where TEntity : IEntity
        where TPrimaryKey : IComparable
    {
        private readonly string TableName;

        private readonly string PrimaryKeyName;

        private QueryFactory QueryFactory { get; }

        public SingleIdCrudRepository(QueryFactory queryFactory, string tableName, string primaryKeyName = "Id")
        {
            QueryFactory = queryFactory;
            TableName = tableName;
            PrimaryKeyName = primaryKeyName;
        }

        /// <inheritdoc />
        public TEntity GetById(TPrimaryKey id)
        {
            return QueryFactory.Query(TableName).Where($"{TableName}.{PrimaryKeyName}", "=", id).FirstOrDefault<TEntity>(null);
        }

        /// <inheritdoc />
        public void Add(object entity)
        {
            QueryFactory.Query(TableName).Insert(entity);
        }

        /// <inheritdoc />
        public TPrimaryKey AddGetId(object entity)
        {
            return QueryFactory.Query(TableName).InsertGetId<TPrimaryKey>(entity);
        }

        /// <inheritdoc />
        public void Remove(TPrimaryKey id)
        {
            QueryFactory.Query(TableName).Where($"{TableName}.{PrimaryKeyName}", "=", id).Delete();
        }

        /// <inheritdoc />
        public void Update(TPrimaryKey id, object entity)
        {
            QueryFactory.Query(TableName).Where($"{TableName}.{PrimaryKeyName}", "=", id).Update(entity);
        }
    }
}