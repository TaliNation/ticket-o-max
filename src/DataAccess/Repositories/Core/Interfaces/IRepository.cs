using System;
using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories.Core
{
    /// <summary>Repository pour les requêtes communes à tous les autres</summary>
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        /// <summary>Recupération de toutes les lignes d'une table</summary>
        IEnumerable<TEntity> GetAll();

        /// <summary>Filtre des données sur une colonne (=)</summary>
        IEnumerable<TEntity> GetByColumn(string columnName, IComparable value);

        /// <summary>Filtre des données sur plusieurs colonnes (=)</summary>
        /// <param name="columnValuePairs">Dictionnaire avec <see cref="P:System.Collections.Generic.Dictionary`2.Keys" /> = nom de la colonne, et <see cref="P:System.Collections.Generic.Dictionary`2.Values" /> = valeur du filtre</param>
        IEnumerable<TEntity> GetByColumn(Dictionary<string, IComparable> columnValuePairs);
    }
}