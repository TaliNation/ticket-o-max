using System;
using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories.Core
{
    /// <summary>Repository de requêtes Create/Read/Update/Delete</summary>
    /// <seealso href="https://martinfowler.com/eaaCatalog/repository.html" />
    public interface ICrudRepository<TEntity, TPrimaryKey>
        where TEntity : IEntity
        where TPrimaryKey : IComparable
    {
        TEntity GetById(TPrimaryKey id);

        /// <summary>Ajout d'une ligne en base de données.</summary>
        /// <param name="entity">Objet mappé sur les noms de champs en base de données</param>
        void Add(object entity);

        /// <inheritdoc cref="Add(object)" />
        /// <returns>L'id de la ligne venant d'être insérée</returns>
        TPrimaryKey AddGetId(object entity);

        /// <summary>Suppression d'une ligne en base de données</summary>
        /// <param name="id">Id de la ligne à modifier</param>
        void Remove(TPrimaryKey id);

        /// <summary>Modification d'une ligne en base de données</summary>
        /// <param name="id">Id de la ligne à modifier</param>
        /// <param name="entity">Objet mappé sur les noms de champs en base de données</param>
        void Update(TPrimaryKey id, object entity);
    }
}