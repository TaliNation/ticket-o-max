using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table PRIVILEGE</summary>
    public interface IPrivilegeRepository
    {
        IRepository<PrivilegeEntity> General { get; }
        ICrudRepository<PrivilegeEntity, int> Crud { get; }
    }
}