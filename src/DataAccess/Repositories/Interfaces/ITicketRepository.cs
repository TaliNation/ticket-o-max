using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table TICKET</summary>
    public interface ITicketRepository
    {
        IRepository<TicketEntity> General { get; }
        ICrudRepository<TicketEntity, int> Crud { get; }
    }
}