using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table INTERVENTION</summary>
    public interface IInterventionRepository
    {
        IRepository<InterventionEntity> General { get; }
        ICrudRepository<InterventionEntity, int> Crud { get; }
    }
}