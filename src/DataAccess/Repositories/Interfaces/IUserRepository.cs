using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table USER</summary>
    public interface IUserRepository
    {
        IRepository<UserEntity> General { get; }
        ICrudRepository<UserEntity, int> Crud { get; }
    }
}