using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la vue VIEW_TICKET</summary>
    public interface ITicketViewRepository
    {
        IRepository<TicketViewEntity> General { get; }

        public IEnumerable<TicketViewEntity> GetAllActivated();
    }
}