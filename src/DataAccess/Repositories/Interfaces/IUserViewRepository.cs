using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la vue VIEW_USER</summary>
    public interface IUserViewRepository
    {
        IRepository<UserViewEntity> General { get; }
    }
}