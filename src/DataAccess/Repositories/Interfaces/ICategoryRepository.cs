using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table CATEGORY</summary>
    public interface ICategoryRepository
    {
        IRepository<CategoryEntity> General { get; }
        ICrudRepository<CategoryEntity, int> Crud { get; }
    }
}