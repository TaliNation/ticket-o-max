using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table SUPPORT</summary>
    public interface ISupportRepository
    {
        IRepository<SupportEntity> General { get; }
        ICrudRepository<SupportEntity, int> Crud { get; }
    }
}