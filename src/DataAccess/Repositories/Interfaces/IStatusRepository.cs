using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table STATUS</summary>
    public interface IStatusRepository
    {
        IRepository<StatusEntity> General { get; }
        ICrudRepository<StatusEntity, int> Crud { get; }
    }
}