using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table EVENT_TYPE</summary>
    public interface IEventTypeRepository
    {
        IRepository<EventTypeEntity> General { get; }
        ICrudRepository<EventTypeEntity, int> Crud { get; }
    }
}