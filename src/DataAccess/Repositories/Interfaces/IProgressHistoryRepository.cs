using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <summary>Accès à la table PROGRESS_HISTORY</summary>
    public interface IProgressHistoryRepository
    {
        IRepository<ProgressHistoryEntity> General { get; }
        ICrudRepository<ProgressHistoryEntity, int> Crud { get; }
    }
}