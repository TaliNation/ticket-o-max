using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IProgressHistoryRepository"/>
    public class ProgressHistoryRepository : IProgressHistoryRepository
    {
        public IRepository<ProgressHistoryEntity> General { get; }
        public ICrudRepository<ProgressHistoryEntity, int> Crud { get; }

        public ProgressHistoryRepository(QueryFactory queryFactory)
        {
            const string tableName = "PROGRESS_HISTORY";

            General = new Repository<ProgressHistoryEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<ProgressHistoryEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}