using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IStatusRepository"/>
    public class StatusRepository : IStatusRepository
    {
        public IRepository<StatusEntity> General { get; }
        public ICrudRepository<StatusEntity, int> Crud { get; }

        public StatusRepository(QueryFactory queryFactory)
        {
            const string tableName = "STATUS";

            General = new Repository<StatusEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<StatusEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}