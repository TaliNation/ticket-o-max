using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IUserViewRepository"/>
    public class UserViewRepository : IUserViewRepository
    {
        private const string TableName = "VIEW_USER";

        public IRepository<UserViewEntity> General { get; }

        public UserViewRepository(QueryFactory queryFactory)
        {
            General = new Repository<UserViewEntity>(queryFactory, TableName);
        }
    }
}