using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IUserRepository"/>
    public class UserRepository : IUserRepository
    {
        public IRepository<UserEntity> General { get; }
        public ICrudRepository<UserEntity, int> Crud { get; }

        public UserRepository(QueryFactory queryFactory)
        {
            const string tableName = "USER";

            General = new Repository<UserEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<UserEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}