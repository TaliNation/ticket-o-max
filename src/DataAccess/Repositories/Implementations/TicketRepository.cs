using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="ITicketRepository"/>
    public class TicketRepository : ITicketRepository
    {
        public IRepository<TicketEntity> General { get; }
        public ICrudRepository<TicketEntity, int> Crud { get; }

        public TicketRepository(QueryFactory queryFactory)
        {
            const string tableName = "TICKET";

            General = new Repository<TicketEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<TicketEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}