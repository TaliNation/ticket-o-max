using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="ICategoryRepository"/>
    public class CategoryRepository : ICategoryRepository
    {
        public IRepository<CategoryEntity> General { get; }

        public ICrudRepository<CategoryEntity, int> Crud { get; }

        public CategoryRepository(QueryFactory queryFactory)
        {
            const string tableName = "CATEGORY";

            General = new Repository<CategoryEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<CategoryEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}