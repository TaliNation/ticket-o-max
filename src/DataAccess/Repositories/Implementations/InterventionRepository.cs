using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IInterventionRepository"/>
    public class InterventionRepository : IInterventionRepository
    {
        public IRepository<InterventionEntity> General { get; }
        public ICrudRepository<InterventionEntity, int> Crud { get; }

        public InterventionRepository(QueryFactory queryFactory)
        {
            const string tableName = "INTERVENTION";

            General = new Repository<InterventionEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<InterventionEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}