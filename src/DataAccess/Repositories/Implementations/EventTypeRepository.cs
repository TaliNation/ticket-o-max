using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IEventTypeRepository"/>
    public class EventTypeRepository : IEventTypeRepository
    {
        public IRepository<EventTypeEntity> General { get; }
        public ICrudRepository<EventTypeEntity, int> Crud { get; }

        public EventTypeRepository(QueryFactory queryFactory)
        {
            const string tableName = "EVENT_TYPE";

            General = new Repository<EventTypeEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<EventTypeEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}