using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="ISupportRepository"/>
    public class SupportRepository : ISupportRepository
    {
        public IRepository<SupportEntity> General { get; }
        public ICrudRepository<SupportEntity, int> Crud { get; }

        public SupportRepository(QueryFactory queryFactory)
        {
            const string tableName = "SUPPORT";

            General = new Repository<SupportEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<SupportEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}