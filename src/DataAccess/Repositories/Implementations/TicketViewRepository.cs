using System.Collections.Generic;
using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="ITicketViewRepository"/>
    public class TicketViewRepository : ITicketViewRepository
    {
        private const string TableName = "VIEW_TICKET";

        private QueryFactory QueryFactory { get; }

        public IRepository<TicketViewEntity> General { get; }

        public IEnumerable<TicketViewEntity> GetAllActivated()
        {
            return QueryFactory.Query(TableName).WhereNull($"{TableName}.DATE_DELETE").OrderByDesc($"{TableName}.DATE_ISSUE").Get<TicketViewEntity>();
        }

        public TicketViewRepository(QueryFactory queryFactory)
        {
            QueryFactory = queryFactory;
            General = new Repository<TicketViewEntity>(queryFactory, TableName);
        }
    }
}