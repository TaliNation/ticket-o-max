using AlfaBureau.TicketOMax.DataAccess.Entities;
using AlfaBureau.TicketOMax.DataAccess.Repositories.Core;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess.Repositories
{
    /// <inheritdoc cref="IPrivilegeRepository"/>
    public class PrivilegeRepository : IPrivilegeRepository
    {
        public IRepository<PrivilegeEntity> General { get; }
        public ICrudRepository<PrivilegeEntity, int> Crud { get; }

        public PrivilegeRepository(QueryFactory queryFactory)
        {
            const string tableName = "PRIVILEGE";

            General = new Repository<PrivilegeEntity>(queryFactory, tableName);
            Crud = new SingleIdCrudRepository<PrivilegeEntity, int>(queryFactory, tableName, $"ID_{tableName}");
        }
    }
}