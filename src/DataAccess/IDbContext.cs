using AlfaBureau.TicketOMax.DataAccess.Repositories;

namespace AlfaBureau.TicketOMax.DataAccess
{
    /// <summary>Interface d'accès à la base de données</summary>
    public interface IDbContext
    {
        /// <inheritdoc cref="ICategoryRepository" />
        ICategoryRepository Category { get; }

        /// <inheritdoc cref="IEventTypeRepository" />
        IEventTypeRepository EventType { get; }

        /// <inheritdoc cref="IInterventionRepository" />
        IInterventionRepository Intervention { get; }

        /// <inheritdoc cref="IPrivilegeRepository" />
        IPrivilegeRepository Privilege { get; }

        /// <inheritdoc cref="IProgressHistoryRepository" />
        IProgressHistoryRepository ProgressHistory { get; }

        /// <inheritdoc cref="IStatusRepository" />
        IStatusRepository Status { get; }

        /// <inheritdoc cref="ISupportRepository" />
        ISupportRepository Support { get; }

        /// <inheritdoc cref="ITicketRepository" />
        ITicketRepository Ticket { get; }

        /// <inheritdoc cref="IUserRepository" />
        IUserRepository User { get; }

        /// <inheritdoc cref="IUserViewRepository" />
        IUserViewRepository UserView { get; }

        /// <inheritdoc cref="ITicketViewRepository" />
        ITicketViewRepository TicketView { get; }
    }
}