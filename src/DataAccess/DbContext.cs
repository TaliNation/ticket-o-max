using System.Data;
using AlfaBureau.TicketOMax.DataAccess.Repositories;
using SqlKata.Compilers;
using SqlKata.Execution;

namespace AlfaBureau.TicketOMax.DataAccess
{
    /// <inheritdoc cref="IDbContext" />
    public class DbContext : IDbContext
    {
        /// <inheritdoc />
        public ICategoryRepository Category { get; }

        /// <inheritdoc />
        public IEventTypeRepository EventType { get; }

        /// <inheritdoc />
        public IInterventionRepository Intervention { get; }

        /// <inheritdoc />
        public IPrivilegeRepository Privilege { get; }

        /// <inheritdoc />
        public IProgressHistoryRepository ProgressHistory { get; }

        /// <inheritdoc />
        public IStatusRepository Status { get; }

        /// <inheritdoc />
        public ISupportRepository Support { get; }

        /// <inheritdoc />
        public ITicketRepository Ticket { get; }

        /// <inheritdoc />
        public IUserRepository User { get; }

        /// <inheritdoc />
        public IUserViewRepository UserView { get; }

        /// <inheritdoc />
        public ITicketViewRepository TicketView { get; }

        private QueryFactory QueryFactory { get; }

        public DbContext(IDbConnection connection)
        {
            QueryFactory = new QueryFactory(connection, new SqlServerCompiler());

            Category = new CategoryRepository(QueryFactory);
            EventType = new EventTypeRepository(QueryFactory);
            Intervention = new InterventionRepository(QueryFactory);
            Privilege = new PrivilegeRepository(QueryFactory);
            ProgressHistory = new ProgressHistoryRepository(QueryFactory);
            Status = new StatusRepository(QueryFactory);
            Support = new SupportRepository(QueryFactory);
            Ticket = new TicketRepository(QueryFactory);
            User = new UserRepository(QueryFactory);
            UserView = new UserViewRepository(QueryFactory);
            TicketView = new TicketViewRepository(QueryFactory);
        }
    }
}