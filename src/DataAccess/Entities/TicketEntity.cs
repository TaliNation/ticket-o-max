using System;

namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table TICKET
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class TicketEntity : IEntity
    {
        public int Id_Ticket { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Id_User_Issue { get; set; }
        public DateTime Date_Issue { get; set; }
        public int? Id_User_Assignee { get; set; }
        public int Id_Status { get; set; }
        public int? Id_User_Resolve { get; set; }
        public DateTime? Date_Resolve { get; set; }
        public int Id_Category { get; set; }
    }
}