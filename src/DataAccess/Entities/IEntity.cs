namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation d'un objet de la base de données
    /// </summary>
    /// <remarks>
    /// Le nom des champs des entités correspondent aux nom des colonnes en base de données. Ils sont insensibles à la casse et doivent respecter la charte de développement quand c'est possible.
    /// </remarks>
    public interface IEntity { }
}