namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table CATEGORY
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class CategoryEntity : IEntity
    {
        public int Id_Category { get; set; }
        public string Label { get; set; }
    }
}