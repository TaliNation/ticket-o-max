namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table STATUS
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class StatusEntity : IEntity
    {
        public int Id_Status { get; set; }
        public string Label { get; set; }
    }
}