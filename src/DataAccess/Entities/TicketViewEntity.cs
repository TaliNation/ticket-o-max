using System;

namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    public class TicketViewEntity : IEntity
    {
        public int Id_Ticket { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int Id_User_Issue { get; set; }
        public string Firstname_User_Issue { get; set; }
        public string Lastname_User_Issue { get; set; }
        public string Email_User_Issue { get; set; }

        public DateTime Date_Issue { get; set; }

        public int? Id_User_Assignee { get; set; }
        public string Firstname_User_Assignee { get; set; }
        public string Lastname_User_Assignee { get; set; }
        public string Email_User_Assignee { get; set; }

        public int Id_Status { get; set; }
        public string Label_Status { get; set; }

        public int? Id_User_Resolve { get; set; }
        public string Firstname_User_Resolve { get; set; }
        public string Lastname_User_Resolve { get; set; }
        public string Email_User_Resolve { get; set; }

        public DateTime? Date_Resolve { get; set; }

        public int Id_Category { get; set; }
        public string Label_Category { get; set; }

        public DateTime? Date_Delete { get; set; }
    }
}