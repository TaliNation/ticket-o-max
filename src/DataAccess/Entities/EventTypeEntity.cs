namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table EVENT_TYPE
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class EventTypeEntity : IEntity
    {
        public int Id_Event_Type { get; set; }
        public string Label { get; set; }
    }
}