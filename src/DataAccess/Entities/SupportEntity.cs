namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table SUPPORT
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class SupportEntity : IEntity
    {
        public int Id_Support { get; set; }
        public string Label { get; set; }
    }
}