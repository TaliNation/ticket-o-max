using System;

namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table INTERVENTION
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class InterventionEntity : IEntity
    {
        public int Id_Intervention { get; set; }
        public int Id_Ticket { get; set; }
        public int Id_User { get; set; }
        public int Id_Support { get; set; }
        public DateTime Date_Intervention { get; set; }
        public string Detail { get; set; }
    }
}