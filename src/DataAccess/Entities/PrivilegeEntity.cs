namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table PRIVILEGE
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class PrivilegeEntity : IEntity
    {
        public int Id_Privilege { get; set; }
        public string Label { get; set; }
    }
}