using System;

namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table PROGRESS_HISTORY
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class ProgressHistoryEntity : IEntity
    {
        public int Id_Progress_History { get; set; }
        public DateTime Date { get; set; }
        public int Id_Event { get; set; }
        public int Id_Ticket { get; set; }
        public int Id_User { get; set; }
    }
}