namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la table USER
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class UserEntity : IEntity
    {
        public int Id_User { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Id_Privilege { get; set; }
    }
}