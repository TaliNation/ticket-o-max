namespace AlfaBureau.TicketOMax.DataAccess.Entities
{
    /// <summary>
    /// Représentation de la vue VIEW_USER
    /// </summary>
    /// <inheritdoc cref="IEntity" />
    public class UserViewEntity : IEntity
    {
        public int Id_User { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Id_Privilege { get; set; }
        public string Label_Privilege { get; set; }
    }
}